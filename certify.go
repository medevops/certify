package certify

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"math/big"
	"net"
	"syscall"
	"time"
)

type Certify struct {
	domain  string
	bitSize int
	rootPEM []byte
	rootCA  *x509.Certificate
	rootKey *rsa.PrivateKey
}

func NewCertify(domain string, bitSize int) *Certify {
	return &Certify{
		domain:  domain,
		bitSize: bitSize,
		rootPEM: nil,
		rootCA:  nil,
		rootKey: nil,
	}
}

func (c *Certify) GetRootCAandPEMandPK() (*x509.Certificate, []byte, *rsa.PrivateKey) {
	return c.rootCA, c.rootPEM, c.rootKey
}

func (c *Certify) GenCert(template *x509.Certificate, publicKey *rsa.PublicKey, privateKey *rsa.PrivateKey) (*x509.Certificate, []byte, error) {
	certBytes, err := x509.CreateCertificate(rand.Reader, template, c.rootCA, publicKey, privateKey)
	if err != nil {
		return nil, nil, fmt.Errorf("[SetRootCA] create certificate: %w", err)
	}

	cert, err := x509.ParseCertificate(certBytes)
	if err != nil {
		return nil, nil, fmt.Errorf("[SetRootCA] parse certificate: %w", err)
	}

	b := pem.Block{Type: "CERTIFICATE", Bytes: certBytes}
	certPEM := pem.EncodeToMemory(&b)

	return cert, certPEM, nil
}

// SetRootCA is trying to load cert and private key
// returns bool showing whether new pair is generated
func (c *Certify) SetRootCA(certFileRaw, keyFileRaw string) (bool, error) {
	var newCert bool
	certFile, err := ioutil.ReadFile(certFileRaw)
	if err != nil {
		if !errors.Is(err, syscall.ENOENT) {
			return false, fmt.Errorf("[SetRootCA] read cert file: %w", err)
		}
	}

	kf, err := ioutil.ReadFile(keyFileRaw)
	if err != nil {
		if !errors.Is(err, syscall.ENOENT) {
			return false, fmt.Errorf("[SetRootCA] read private key file: %w", err)
		}
	}

	if certFile != nil || kf != nil {
		cpb, _ := pem.Decode(certFile)
		kpb, _ := pem.Decode(kf)

		crt, err := x509.ParseCertificate(cpb.Bytes)
		if err != nil {
			return false, fmt.Errorf("[SetRootCA] parse cert file: %w", err)
		}

		key, err := x509.ParsePKCS1PrivateKey(kpb.Bytes)
		if err != nil {
			return false, fmt.Errorf("[SetRootCA] parse private key file: %w", err)
		}

		c.rootCA = crt
		c.rootKey = key

	} else {
		c.rootCA = &x509.Certificate{
			SerialNumber: big.NewInt(1),
			Subject: pkix.Name{
				Country:      []string{"SU"},
				Organization: []string{"Forklifter"},
				CommonName:   "Root CA",
			},
			NotBefore:             time.Now().Add(-10 * time.Second),
			NotAfter:              time.Now().AddDate(10, 0, 0),
			KeyUsage:              x509.KeyUsageCertSign | x509.KeyUsageCRLSign,
			ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
			BasicConstraintsValid: true,
			IsCA:                  true,
			MaxPathLen:            2,
			IPAddresses:           []net.IP{net.ParseIP(c.domain)},
		}

		key, err := rsa.GenerateKey(rand.Reader, c.bitSize)
		if err != nil {
			return false, fmt.Errorf("[SetRootCA] generate private key : %w", err)
		}

		rootCert, rootPEM, err := c.GenCert(c.rootCA, &key.PublicKey, key)
		if err != nil {
			return false, fmt.Errorf("[SetRootCA] generate root CA and PEM: %w", err)
		}

		c.rootCA = rootCert
		c.rootKey = key
		c.rootPEM = rootPEM
		newCert = true
	}

	return newCert, nil
}

func (c *Certify) GenDCA(client string) (*x509.Certificate, []byte, *rsa.PrivateKey, error) {
	priv, err := rsa.GenerateKey(rand.Reader, c.bitSize)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("[GenDCA] generate client private key: %w", err)
	}

	var DCATemplate = x509.Certificate{
		SerialNumber: big.NewInt(1),
		Subject: pkix.Name{
			Country:      []string{"SU"},
			Organization: []string{client},
			CommonName:   "DCA",
		},
		NotBefore:             time.Now().Add(-10 * time.Second),
		NotAfter:              time.Now().AddDate(10, 0, 0),
		KeyUsage:              x509.KeyUsageCertSign | x509.KeyUsageCRLSign,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
		IsCA:                  true,
		MaxPathLenZero:        false,
		MaxPathLen:            1,
		IPAddresses:           []net.IP{net.ParseIP("127.0.0.1")},
	}
	DCACert, DCAPEM, err := c.GenCert(&DCATemplate, &priv.PublicKey, c.rootKey)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("[GenDCA] generate client cert and PEM: %w", err)
	}

	return DCACert, DCAPEM, priv, nil
}

func (c *Certify) GenServerCert(DCAKey *rsa.PrivateKey) (*x509.Certificate, []byte, *rsa.PrivateKey, error) {
	priv, err := rsa.GenerateKey(rand.Reader, c.bitSize)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("[GenServerCert] generate client private key: %w", err)
	}

	var ServerTemplate = x509.Certificate{
		SerialNumber:   big.NewInt(1),
		NotBefore:      time.Now().Add(-10 * time.Second),
		NotAfter:       time.Now().AddDate(10, 0, 0),
		KeyUsage:       x509.KeyUsageCRLSign,
		ExtKeyUsage:    []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		IsCA:           false,
		MaxPathLenZero: true,
		IPAddresses:    []net.IP{net.ParseIP("127.0.0.1")},
	}

	ServerCert, ServerPEM, err := c.GenCert(&ServerTemplate, &priv.PublicKey, DCAKey)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("[GenServerCert] generate client private key: %w", err)
	}

	return ServerCert, ServerPEM, priv, nil
}

func (c *Certify) VerifyDCA(dca *x509.Certificate) error {
	roots := x509.NewCertPool()
	roots.AddCert(c.rootCA)
	opts := x509.VerifyOptions{
		Roots: roots,
	}

	if _, err := dca.Verify(opts); err != nil {
		return fmt.Errorf("[VerifyDCA] verify cert: %w", err)
	}

	return nil
}

func (c *Certify) VerifyLow(DCA, child *x509.Certificate) error {
	roots := x509.NewCertPool()
	inter := x509.NewCertPool()
	roots.AddCert(c.rootCA)
	inter.AddCert(DCA)
	opts := x509.VerifyOptions{
		Roots:         roots,
		Intermediates: inter,
	}

	if _, err := child.Verify(opts); err != nil {
		return fmt.Errorf("[VerifyLow] verify low: %w", err)
	}

	return nil
}
